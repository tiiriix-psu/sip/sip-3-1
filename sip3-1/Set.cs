﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

abstract class Set<T> where T: IConvertible
{
    private readonly List<T> values = new List<T>();

    public List<T> Values => values;

    public abstract void Add(T value);

    public abstract void Remove(T value);

    public abstract bool Exist(T value);

    public void Fill(String str)
    {
        foreach (var numStr in str.Split(' '))
        {
            Add(Convert.ToInt32(numStr));
        }
    }

    public void Fill(int[] array)
    {
        foreach (var value in array)
        {
            Add(value);
        }
    }

    public void Print()
    {
        Console.WriteLine(String.Join(" ", values));
    }
}